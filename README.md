## Nearby venues

### General requirements

- get nearby venues to eat/drink using foursquare API. 
- save basic restaurants data to database (Name, Photo, Lat/Lng, Distance, Open/closed, Website)
- return basic restaurant data for the user location from database, with the distance for each restaurant from user
- If data is available in database for a lat/lng with default radius, then return that data

### Technical requirements
 - [ ] Http-caching
 - [ ] do not duplicate restaurants in database
 - [ ] use New York as a sample city (high density of venues)
